Source: pg-rational
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/begriffs/pg_rational
Vcs-Browser: https://salsa.debian.org/postgresql/pg-rational
Vcs-Git: https://salsa.debian.org/postgresql/pg-rational.git

Package: postgresql-17-rational
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: Precise fractional arithmetic for PostgreSQL
 pg_rational implements precise fractional arithmetic for PostgreSQL. Perfect
 for exact arithmetic or user-specified table row ordering. Holds values as big
 as an integer, with matching precision in the denominator.
 .
 Features:
  * Stores fractions in exactly 64 bits (same size as float)
  * Written in C for high performance
  * Detects and halts arithmetic overflow for correctness
  * Uses native CPU instructions for fast overflow detection
  * Defers GCD calculation until requested or absolutely required
  * Supports btree and hash indices
  * Implements Stern-Brocot trees for finding intermediate points
  * Coercion from integer/bigint/tuple
  * Custom aggregate
